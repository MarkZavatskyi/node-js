const { user } = require('../models/user');
const createUserValid = (req, res, next) => {
    const params = req.body;

    if (req.body.id) {
        return res.status(400).json({
           error: true,
           message: "id field should not be in request body!"
        });
    }

    for (let field in user) {
        if (!params.hasOwnProperty(field) && field !== 'id') {
            return res.status(400).json({
                error: true,
                message: `required field ${field} not found!`
            });
        }
    }


    for (let param in params) {

        if (!user.hasOwnProperty(param)) {
            return res.status(400).json({
                error: true,
                message: "extra fields found!"
            });
        }

        const pattern = new RegExp(user[param]);
        const result = pattern.test(params[param]);

        if (!result) {
            return res.status(400).json({
                error: true,
                message: `${param} field not valid!`
            });
        }
    }

    next();
}

const updateUserValid = (req, res, next) => {
    const params = req.body;

    if (req.body.id) {
        return res.status(400).json({
            error: true,
            message: "id field should not be in request body!"
        });
    }

    for (let param in params) {

        if (!user.hasOwnProperty(param)) {
            return res.status(400).json({
                error: true,
                message: "extra fields found!"
            });
        }

        const pattern = new RegExp(user[param]);
        const result = pattern.test(params[param]);

        if (!result) {
            return res.status(400).json({
                error: true,
                message: `${param} field not valid!`
            });
        }
    }

    next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;