const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {
    if (req.body.id) {
        return res.status(400).json({
            error: true,
            message: "id field should not be in request body!"
        });
    }

    const params = req.body;

    for (let field in fighter) {
        if (!params.hasOwnProperty(field) && field !== 'id') {
            return res.status(400).json({
                error: true,
                message: `required field ${field} not found!`
            });
        }
    }

    for (let param in params) {
        if (!fighter.hasOwnProperty(param)) {
            return res.status(400).json({
                error: true,
                message: "extra fields found!"
            });
        }

        const validationParams = String(fighter[param]).split('|');
        const validate = validateRequestFields(params, param, validationParams);

        if (validate.error_message) {
            return res.status(400).json({
                error: true,
                message: validate.error_message
            });
        }
    }

    Object.keys(fighter).forEach(field => {
        if (!params.hasOwnProperty(field)) {
            req.body[field] = fighter[field];
        }
    });

    next();
}

const updateFighterValid = (req, res, next) => {
    if (req.body.id) {
        return res.status(400).json({
            error: true,
            message: "id field should not be in request body!"
        });
    }

    const params = req.body;

    for (let param in params) {
        if (!fighter.hasOwnProperty(param)) {
            return res.status(400).json({
                error: true,
                message: "extra fields found!"
            });
        }

        const validationParams = String(fighter[param]).split('|');
        const validate = validateRequestFields(params, param, validationParams);

        if (validate.error_message) {
            return res.status(400).json({
                error: true,
                message: validate.error_message
            });
        }
    }

    next();
}

const validateRequestFields = (params, param, validationParams) => {
    let error_message = null;

    validationParams.forEach(validationField => {
        const [key, value] = validationField.split(':');

        switch (key) {
            case 'min':
                if (+params[param] < +value) {
                    error_message = `${param} value is less than the minimum!`;
                }
                break;
            case 'max':
                if (+params[param] > +value) {
                    error_message = `${param} value is greater than the maximum!`;
                }
                break;
            case 'pattern':
                const pattern = new RegExp(value);
                if (!pattern.test(String(params[param]))) {
                    error_message = `${param} field not valid!`;
                }
                break;
            case 'type':
                if (typeof params[param] !== value) {
                    error_message = `${param} must be ${value}`
                }
                break;
        }
    });

    if (error_message) {
        return {
            error_message
        }
    }

    return true;
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;