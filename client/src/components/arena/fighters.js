import React, { Component } from "react";
import fighterImg from  "../../defaultFighter.webp";

export default class Fighters extends Component {

    render(){
        return(
            <div className="arena___battlefield">
                <div className="arena___fighter arena___left-fighter">
                    <img src={fighterImg} alt="leftFighter"/>
                </div>
                <div className="arena___fighter arena___right-fighter">
                    <img src={fighterImg} alt="rightFighter"/>
                </div>
            </div>
        );
    }
}