import React, { Component } from 'react';
import HealthIndicators from "../healthIndicators";
import Fighters from "./fighters";
import Modal from "../modal";
import { controls } from '../../constants/controls';
import "./arena.css";

export default class Arena extends Component {

    state = {
        showModal: false,
        winner: null,
    }

    showWinnerModal() {
        const { winner } = this.state;

        if (!winner) {
            return false;
        }

        const modalBody = <h1>{ winner.name } win!</h1>;

        return <Modal title={`The fight is over!`} modalBody={modalBody} onClose={this.props.fightIsOver}/>;
    }

    getDamage(attacker, defender) {
        let damage = this.getHitPower(attacker) - this.getBlockPower(defender);

        if (damage < 0) {
            damage = 0;
        }

        return damage
    }

    getHitPower(fighter) {
        const min = 1;
        const max = 2;
        const criticalHitChance = Math.random() + (max - min);

        return fighter.power * criticalHitChance;
    }

    getBlockPower(fighter) {
        const min = 1;
        const max = 2;
        const dodgeChance = Math.random() + (max - min);

        return fighter.defense * dodgeChance;
    }

    changeFighterHpIndicator(side, defaultHp, hpNow) {
        const indicator = document.getElementById(`${side}-fighter-indicator`);
        if (hpNow > 0) {
            indicator.style.width = `${hpNow / (defaultHp / 100)}%`;
        } else {
            indicator.style.width = 0;
        }
    }

    fight(firstFighter, secondFighter) {
        let firstHp = firstFighter.health;
        let secondHp = secondFighter.health;

        let firstBlock = false;
        let secondBlock = false;

        let codes = new Set();
        let firstCritCooldown = false;
        let secondCritCooldown = false;

        window.addEventListener('keydown', (e) => {
            switch (e.code) {
                case controls.PlayerOneAttack:
                    if (!secondBlock && !firstBlock) {
                        secondHp -= this.getDamage(firstFighter, secondFighter);
                        this.changeFighterHpIndicator('right', secondFighter.health, secondHp);
                    }
                    break;
                case controls.PlayerTwoAttack:
                    if (!firstBlock && !secondBlock) {
                        firstHp -= this.getDamage(secondFighter, firstFighter);
                        this.changeFighterHpIndicator('left', firstFighter.health, firstHp);
                    }
                    break;
                case controls.PlayerOneBlock:
                    firstBlock = true;
                    break;
                case controls.PlayerTwoBlock:
                    secondBlock = true;
                    break;
            }
        });

        window.addEventListener('keyup', (e) => {
            switch (e.code) {
                case controls.PlayerOneBlock:
                    firstBlock = false;
                    break;
                case controls.PlayerTwoBlock:
                    secondBlock = false;
                    break;
            }
        });

        window.addEventListener('keydown', (e) => {
            codes.add(e.code);

            for (let code of controls.PlayerOneCriticalHitCombination) {
                if(!codes.has(code)) {
                    return;
                }
            }

            codes.clear();

            if (!firstCritCooldown) {
                secondHp -= 2 * firstFighter.power;
                this.changeFighterHpIndicator('right', secondFighter.health, secondHp);

                firstCritCooldown = true;
                setTimeout(() => { firstCritCooldown = false }, 10000);
            }
        });

        window.addEventListener('keydown', (e) => {
            codes.add(e.code);

            for (let code of controls.PlayerTwoCriticalHitCombination) {
                if(!codes.has(code)) {
                    return;
                }
            }

            codes.clear();

            if (!secondCritCooldown) {
                firstHp -= 2 * secondFighter.power;
                this.changeFighterHpIndicator('left', firstFighter.health, firstHp);

                secondCritCooldown = true;
                setTimeout(() => { secondCritCooldown = false }, 10000);
            }
        });

        document.addEventListener('keyup', (e) => {
            codes.delete(e.code);
        });

        return new Promise((resolve) => {
            setInterval(() => {
                if (firstHp < 0) {
                    resolve(secondFighter);
                }

                if (secondHp < 0) {
                    resolve(firstFighter);
                }
            }, 500);
        });
    }

    render() {
        const [firstFighter, secondFighter] = this.props.fighters;
        this.fight(firstFighter, secondFighter)
            .then(winner => {
                this.setState({ winner: winner, showModal: true });
            });

        return(
            <div className="arena___root">
                { !this.state.showModal || this.showWinnerModal() }
                <HealthIndicators fighters={this.props.fighters}/>
                <Fighters />
            </div>
        );
    }
}