import * as React from 'react';
import SignInUpPage from '../signInUpPage';
import { isSignedIn } from '../../services/authService';
import Fight from '../fight';
import Arena from "../arena";
import SignOut from '../signOut';

class StartScreen extends React.Component {
    state = {
        isSignedIn: false,
        fightStarted: false,
        fighters: null
    };

    componentDidMount() {
        this.setIsLoggedIn(isSignedIn());
    }

    setIsLoggedIn = (isSignedIn) => {
        this.setState({ isSignedIn });
    }

    startFight = (fighter1, fighter2) => {
        this.setState({
            fightStarted: true,
            fighters: [fighter1, fighter2]
        });
    }

    render() {
        const { isSignedIn } = this.state;
        if (!isSignedIn) {
            return <SignInUpPage setIsLoggedIn={this.setIsLoggedIn} />
        }

        return (
            <>
                { this.state.fightStarted ?
                    <Arena fighters={this.state.fighters} fightIsOver={() => this.setState({ fightStarted: false })} />
                    :
                    <>
                        <Fight startFight={this.startFight} />
                        <SignOut isSignedIn={isSignedIn} onSignOut={() => this.setIsLoggedIn(false)} />
                    </>
                }
            </>
        );
    }
}

export default StartScreen;