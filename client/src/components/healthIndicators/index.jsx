import React, { Component } from 'react';
import Indicator from "./indicator";

export default class HealthIndicator extends Component {

    render() {
        const [fighter1, fighter2] = this.props.fighters;

        return(
            <div className="arena___fight-status">
                <Indicator fighter={fighter1} position="left" />
                <div className="arena___versus-sign"/>
                <Indicator fighter={fighter2} position="right" />
            </div>
        );
    }
}