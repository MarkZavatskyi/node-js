import React, { Component } from 'react';

export default class Indicator extends Component {

    render() {
        const { name } = this.props.fighter;

        return(
            <div className="arena___fighter-indicator">
                <span className="arena___fighter-name">{name}</span>
                <div className="arena___health-indicator">
                    <div id={`${this.props.position}-fighter-indicator`} className="arena___health-bar"/>
                </div>
            </div>
        );
    }
}