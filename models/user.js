exports.user = {
    id: '',
    firstName: '',
    lastName: '',
    email: '\\w+@gmail.com',
    phoneNumber: '\\+380[0-9]{9}',
    password: '[a-zA-Z0-9]{3,20}' // min 3 symbols
}