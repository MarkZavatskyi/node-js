const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    all() {
        return FighterRepository.getAll();
    }

    search(search) {
        const item = FighterRepository.getOne(search);

        if(!item) {
            return null;
        }
        return item;
    }

    getById(id) {
        return this.search({ id });
    }

    create(data) {
        return FighterRepository.create(data);
    }

    update(id, data) {
        return FighterRepository.update(id, data);
    }

    remove(id) {
        return FighterRepository.delete(id);
    }
}

module.exports = new FighterService();