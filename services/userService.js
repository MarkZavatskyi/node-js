const { UserRepository } = require('../repositories/userRepository');

class UserService {

    all() {
        return UserRepository.getAll();
    }

    search(search) {
        const item = UserRepository.getOne(search);

        if(!item) {
            return null;
        }
        return item;
    }

    getById(id) {
        return this.search({ id });
    }

    create(data) {
        return UserRepository.create(data);
    }

    update(id, data) {
        return UserRepository.update(id, data);
    }

    remove(id) {
        return UserRepository.delete(id);
    }
}

module.exports = new UserService();