const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

router.get('/', (req, res) => {
    const fighters = FighterService.all();

    res.status(200).json(fighters);
});

router.get('/:id', (req, res) => {
    const { id } = req.params;
    const fighter = FighterService.getById(id);

    res.status(200).json(fighter);
});

router.post('/', createFighterValid, (req, res) => {
    const candidate = FighterService.search({ name: req.body.name });

    if (candidate) {
        return res.status(400).json({
            error: true,
            message: "Fighter already exists!"
        });
    }

    const fighter = FighterService.create(req.body);
    res.status(200).json(fighter);
});

router.put('/:id', updateFighterValid, (req, res) => {
    const { id } = req.params;
    const candidate = FighterService.getById(id);

    if (!candidate) {
        return res.status(400).json({
            error: true,
            message: "Undefined fighter!"
        })
    }
    const fighter = FighterService.update(id, req.body);

    res.status(200).json(fighter);
});

router.delete('/:id', (req, res) => {
    const { id } = req.params;

    const fighter = FighterService.getById(id);

    if (!fighter) {
        return res.status(404).json({
            error: true,
            message: "Fighter not found!"
        });
    }

    FighterService.remove(id);

    res.status(200).json({ removed_id: id });
});

module.exports = router;