const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.get('/', (req, res) => {
    const users = UserService.all();

    res.status(200).json(users);
});

router.get('/:id', (req, res) => {
    const { id } = req.params;
    const user = UserService.getById(id);

    res.status(200).json(user);
});

router.post('/', createUserValid, (req, res) => {
    const { email, phoneNumber } = req.body;

    if (UserService.search({ phoneNumber }) || UserService.search({ email })) {
        return res.status(400).json({
            error: true,
            message: 'User already exists!'
        });
    }

    const user = UserService.create(req.body);
    res.status(200).json(user);
});

router.put('/:id', updateUserValid, (req, res) => {
    const { id } = req.params;

    const candidate = UserService.getById(id);

    if (!candidate) {
        return res.status(400).json({
            error: true,
            message: "Undefined user!"
        })
    }

    const user = UserService.update(id, req.body);

    res.status(200).json(user);
});

router.delete('/:id', (req, res) => {
    const { id } = req.params;

    const user = UserService.getById(id);

    if (!user) {
        return res.status(404).json({
            error: true,
            message: "User not found!"
        });
    }

    UserService.remove(id);

    res.status(200).json({
       removed_id: id,
    });
});

module.exports = router;